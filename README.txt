
Project: PDF Reader v1.0
Objective:
This project is mainly to create "Class / Object" read bank statement of PDF file.
It read the pdf and format it to readable json text format. and give back to the requester.
In this normal kes eboss app will be the requester. 
It will get the result for furthor processing of bank reconcilation in eboss app.
This can be add in the gateway/miroservice. It will not include business logic.
** need to make sure on security so that the pdf bank statement not accessibile to outsider.
** need to do many processing since each statement bank is defererence base on bank.

Technologies:
=> 2024-05-01 => python3, docker, pdfplumber (for pdf processing)
=> 2024-05-21 => add => flask (for web service)

Historical:
=> Failed => use Tesseract => install locally very complicated dependancy. Stop trying.
=> Failed => use comelot => got error on deprecated. 
It appears that Camelot-py still relies on PyPDF2 internally for PDF parsing,
which is causing the same DeprecationError related to PdfFileReader.
Unresolve using chatgpt

=> Success => use pdfplumber => Easy

If your Python container is exiting immediately after starting when you use docker-compose up,
it might be due to the absence of any long-running process.

When you run docker-compose up, it starts all the services defined in your docker-compose.yml file,
and these services usually need to run continuously to keep the container alive.
------------------


#FIRST TIME => Build the containter of python and al it dependency
docker compose build

#Run it to enter the bash container
docker compose run python bash

#Run the program. Hardcode
root@ba4e1ab61b11:/app# python3 main.py

